import { FirstCompComponent } from "./first-comp/first-comp.component";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";

import { AppRoutingModule, routingComponents } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { SecondCompComponent } from "./second-comp/second-comp.component";
import { ApiCallingComponent } from './api-calling/api-calling.component';

@NgModule({
  declarations: [
    AppComponent,
    routingComponents,
    FirstCompComponent,
    SecondCompComponent,
    ApiCallingComponent
  ],
  imports: [BrowserModule, AppRoutingModule, FormsModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
