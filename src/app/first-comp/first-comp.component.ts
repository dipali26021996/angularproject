import { Customer } from "./../model/Customer";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-first-comp",
  templateUrl: "./first-comp.component.html",
  styleUrls: ["./first-comp.component.css"]
})
export class FirstCompComponent implements OnInit {
  customer: Customer;
  constructor() {}
  saveData() {
    alert(JSON.stringify(this.customer));
  }

  ngOnInit(): void {
    this.customer = new Customer();
    this.customer.firstName = "dipa";
    this.customer.lastName = "chaple";
  }
}
