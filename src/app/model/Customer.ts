export class Customer {
  firstName: string;
  lastName: string;
  emailId: string;
  MobileNum: number;
  address: string;
}
