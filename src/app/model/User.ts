export class User {
  firstName: string;
  lastName: string;
  mobileNumber: number;
  emailId: string;
  accountType: string;
  dateOfBirth: number;
  bankId: number;
  branchId: number;
  accountNumber: number;
  address: string;
}
