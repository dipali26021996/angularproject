import { User } from "./../model/User";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-api-calling",
  templateUrl: "./api-calling.component.html",
  styleUrls: ["./api-calling.component.css"],
})
export class ApiCallingComponent implements OnInit {
  user: User;
  constructor() {}
  saveData() {
    console.log(JSON.stringify(this.user));
  }

  ngOnInit(): void {
    this.user = new User();
  }
}
