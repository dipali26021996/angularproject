import { ApiCallingComponent } from "./api-calling/api-calling.component";
import { SecondCompComponent } from "./second-comp/second-comp.component";
import { FirstCompComponent } from "./first-comp/first-comp.component";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [
  { path: "firstComp", component: FirstCompComponent },
  { path: "secondComp", component: SecondCompComponent },
  { path: "ApiCalling", component: ApiCallingComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
export const routingComponents = [
  FirstCompComponent,
  SecondCompComponent,
  ApiCallingComponent,
];
